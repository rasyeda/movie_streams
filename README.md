# movie_streams

Movie catalog app using TMDB API.
This project implements BLoC architecture using stream (RxDart) and StreamBuilder providing infinite list of favorite movies.