import 'package:flutter/material.dart';

import 'package:movie_streams/blocs/bloc_provider.dart';
import 'package:movie_streams/blocs/movie_catalog_bloc.dart';
import 'package:movie_streams/pages/list.dart';
import 'package:movie_streams/pages/list_one_page.dart';
import 'package:movie_streams/widgets/favorite_button.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My Movies')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              child: Text('Movie List'),
              onPressed: () => _openPage(context),
            ),
            FavoriteButton(
              child: Text('Favorite Movies'),
            ),
            RaisedButton(
              child: Text('One Page'),
              onPressed: () => _openOnePage(context),
            ),
          ],
        ),
      ),
    );
  }

  void _openPage(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (BuildContext builder) {
        return BlocProvider<MovieCatalogBloc>(
          bloc: MovieCatalogBloc(),
          child: ListPage(),
        );
      }),
    );
  }

  void _openOnePage(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (BuildContext context) {
        return BlocProvider<MovieCatalogBloc>(
          bloc: MovieCatalogBloc(),
          child: ListOnePage(),
        );
      }),
    );
  }
}
